# Networking

Networking is a big topic.

## Client
```go
conn, err := net.Dial("tcp", "localhost:3333")
defer conn.Close()
conn.Write([]byte("Hello there"))
```

## TCP Server
```go
serv, err := net.Listen("tcp", "localhost:3333")

for {
    conn, err := serv.Accept()
    defer conn.Close()

}
```

## UDP Server
```go
addr, err := net.ResolveUDPAddr("udp", "localhost:3333")
serv, err := net.ListenUDP("udp", addr)

for {
    readBytes, conn, err := serv.ReadFromUDP(buf)
    defer conn.Close()
}
```

