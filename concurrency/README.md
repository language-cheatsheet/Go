# Concurrency in Go

Concurrency in other languages use threads, whereas in Golang 'channels' and 'goroutines' are used. 

A `goroutine` is a function that would be spawned off in a thread.
A `channel` is essentially a queue that data can be pushed into and pulled out of from threads. This is how go handles race conditions with threads.

## Starting a Goroutine
To start a goroutine, you can either create an anonymous function or use an existing one, call it with the `go` prefix. For instance:
```golang
for _, name := range names {
    go func(name string) {
        fmt.Println("Hello, %s", name)
    }(name)
}
```
This will start a new thread for each name in `names` using an anonymous function.

## Using Channels
To use a channel, first you must add data to the channel
```golang
resultChannel := make(chan string)

for _, name := range names {
    go func(name string) {
        resultChannel <- "Hello, " + name
    }(name)
}
```

To pull data out of the channel you must loop over the same number of results that went into the channel:
```golang
for _, name := range names {
    r := <- resultChannel
    fmt.Println(r)
}
```
