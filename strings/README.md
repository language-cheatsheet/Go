# Strings

Strings in go are pretty much like strings in any other strictyped language, such as c

Strings are defined with double quotes `"`, not single quotes `'`. 

## Multi line strings
To define a multiline string, you can use `. 
```golang
multiLine :=
		`hello
	how are ya now
	good n you
	oh not so bad`
```

## String Length
There is the `len` function, or `runeCount` which is sorta different.
```golang
helloStr := "hello, World!"
strLen := len(helloStr)
```

## Split a String
To split a string into a list, by a given delimiter:
```golang
lines := strings.Split(multiLine, "\n")
```

## Join a list into a String
```golang
strings.Join(lines, "\n"))
```

## Convert to String
```go
// convert a number to a string
num := 5
numStr := strconv.Itoa(5)
```

## Generate a string of length
In Python you can do something like
```python
print('-' * 5)
# output: -----
```
golang cannot do this, so you need a handy func:
```go
func genStringOfLen(c string, len int) string {
	if len < 1 {
		return ""
	}
	ret := ""
	for i := 0; i < len; i++ {
		ret += c
	}
	return ret
}
```

## Get max length of string in list
```go
func getMaxLengthString(input []string) int {
	max := -2
	for _, s := range input {
		if len(s) < max {
			// string is shorter, move on
			continue
		}
		if len(s) > max {
			// string is longer, time to dethrone the king
			max = len(s)
		}
	}
	return max
}
```