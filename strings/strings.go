package strings

import (
	"fmt"
	"strings"
)

/*
	generate a string of character @c of a given length @len
	i.e.:
		c = '-', len = 5
		-----
*/
func genStringOfLen(c string, len int) string {
	if len < 1 {
		return ""
	}
	ret := ""
	for i := 0; i < len; i++ {
		ret += c
	}
	return ret
}

/*
	get the length of the longest string in an array
	return: the length of the longest string
*/
func getMaxLengthString(input []string) int {
	max := -2
	for _, s := range input {
		if len(s) < max {
			// string is shorter, move on
			continue
		}
		if len(s) > max {
			// string is longer, time to dethrone the king
			max = len(s)
		}
	}
	return max
}

func main() {
	// multiline strings, newline is preserved
	multiLine :=
		`hello
	how are ya now
	good n you
	oh not so bad`
	fmt.Println(multiLine)
	fmt.Println("------")

	// get the length of a string
	multiLineLen := len(multiLine)
	fmt.Println(multiLineLen)
	fmt.Println("------")

	// split a string into a list
	lines := strings.Split(multiLine, "\n")
	fmt.Println(lines)
	fmt.Println("------")

	// join a list into a string
	fmt.Println(strings.Join(lines, "\n"))

}
