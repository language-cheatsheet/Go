# Testing in Go
There's a lot to this topic, but here are some basics.

Test files must be named after the file they test, with `_test` appended.
```
networking.go
networking_test.go
```

## Helper Functions
You can create helper functions to get rid of some of the bloat of testing. A helper function is defined within your test function, and assigned to a variable that you can then call (function pointer)

```go
func TestSomeFuncion(t *testing.T) {
	checkFunc := func(t testing.TB, want string, expectErr bool) {
		t.Helper()
		got, err := someFunc()
		if (err != nil) != expectErr {
			t.Errorf("got\n%q\nwant\n%q", got, want)
		}
	}
    checkFunc(t, "bananas", false)
}
```

## Making sure you got an error or didn't, and that was correct
If you want to test if you should have gotten an error, or shouldn't have gotten one, pass in a bool to your helper function, then check against the result of `err != nil`:
```go
if (err != nil) != expectErr {
    // we shouldn't have gotten whatever happened
}
```

## Sub Tests
You can run subtests within a test, to do iterations.
```go
t.Run("BasicCheck", func(t *testing.T) {
		checkFunc(t, "bananas", false)
	})

t.Run("Failure Check", func(t *testing.T) {
		checkFunc(t, "", true)
	})
```