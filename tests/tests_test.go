package tests

import "testing"

func someFunc() (string, error) { return "ah", nil }

func TestSomeFuncion(t *testing.T) {
	checkFunc := func(t testing.TB, want string, expectErr bool) {
		t.Helper()
		got, err := someFunc()
		if (err != nil) != expectErr {
			t.Errorf("got\n%q\nwant\n%q", got, want)
		}
	}

	t.Run("BasicCheck", func(t *testing.T) {
		checkFunc(t, "bananas", false)
	})
}
